#
# File specifying the location of Herwig to use.
#

set( HERWIG_LCGVERSION 6.520.2 )
set( HERWIG_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/herwig/${HERWIG_LCGVERSION}/${LCG_PLATFORM} )
